FROM node:10
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
ENV NODE_ENV production
CMD ["npm", "run", "build"]

FROM nginx
COPY ./build /usr/share/nginx/html
