import React from 'react';
import ReactDOM from 'react-dom'
import sinon from 'sinon'
import thunk from 'redux-thunk'

import configureStore from 'redux-mock-store'

import { Provider } from 'react-redux'

import ConnectedApp from './App'
import * as actions from './state/actions'

const mockStore = configureStore([thunk])

const stubs = {};

beforeAll(() => {
  stubs.initSocket = sinon.stub(actions, 'initSocket').returns(() => {
    return function(dispatch) {}
  })
  stubs.initWebLNProvider = sinon.stub(actions, 'initWebLNProvider').returns(() => {
    return async function(dispatch) {}
  })
})

afterAll(() => {
  stubs.initSocket.restore()
  stubs.initWebLNProvider.restore()
})

it('renders and calls initSocket', () => {
  const state = {
    authenticated: {id: ''},
    visibilityFilter: actions.VisibilityFilters.SHOW_INITIALIZING
  }
  const store = mockStore(state);
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedApp/>
    </Provider>,
    div
  );
  expect(stubs.initSocket.called).toEqual(true)
  expect(stubs.initWebLNProvider.called).toEqual(true)
});