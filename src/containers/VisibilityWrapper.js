import React from 'react'
import PropTypes from 'prop-types'

const VisibilityWrapper = ({ active, children }) => {
  if (active) {
    return (
        <span>
          {children}
        </span>
      )
  }

  return <span style={{display: "none", visibility: "hidden"}}></span>
}

VisibilityWrapper.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
}

export default VisibilityWrapper