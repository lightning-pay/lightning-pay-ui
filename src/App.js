import React, {Component} from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import './App.css';

import Initializing from './components/Initializing';
import KeyAuth from './components/KeyAuth'
import CreateBasicAuth from './components/CreateBasicAuth'
import BasicAuth from './components/BasicAuth'
import Navigation from './components/Navigation';
import PaymentInitiate from './components/PaymentInitiate';
import PaymentRequest from './components/PaymentRequest';
import PaymentAccepted from './components/PaymentAccepted';
import PaymentCanceled from './components/PaymentCanceled';
import PaymentExpired from './components/PaymentExpired';
import PaymentHistory from './components/PaymentHistory';
import PaymentHistoryDetail from './components/PaymentHistoryDetail';
import ShowError from './components/ShowError';

import VisibleElement from './components/VisibleElement';

import * as actions from './state/actions'

export class App extends Component {
  constructor(props) {
    super(props)
    this.initPayment = this.initPayment.bind(this)
  }

  initPayment() {
    this.props.dispatch(actions.requestCard())
  }

  render() {
    return (
      <div>
        <Navigation initPayment={this.initPayment} visibilityFilter={this.props.visibilityFilter}/>

        <div className="container-fluid">
          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_INITIALIZING}>
            <Initializing></Initializing>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_KEY_AUTH}>
            <KeyAuth></KeyAuth>
          </VisibleElement>
          
          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_CREATE_BASIC_AUTH}>
            <CreateBasicAuth></CreateBasicAuth>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_BASIC_AUTH}>
            <BasicAuth></BasicAuth>
          </VisibleElement>
          
          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_INIT_PAYMENT_REQUEST}>
            <PaymentInitiate></PaymentInitiate>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_PAYMENT_REQUEST}>
            <PaymentRequest></PaymentRequest>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_PAYMENT_ACCEPTED}>
            <PaymentAccepted></PaymentAccepted>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_PAYMENT_CANCELED}>
            <PaymentCanceled></PaymentCanceled>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_PAYMENT_EXPIRED}>
            <PaymentExpired></PaymentExpired>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_PAYMENT_HISTORY}>
            <PaymentHistory></PaymentHistory>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_PAYMENT_HISTORY_DETAIL}>
            <PaymentHistoryDetail></PaymentHistoryDetail>
          </VisibleElement>

          <VisibleElement active={this.props.visibilityFilter === actions.VisibilityFilters.SHOW_ERROR}>
            <ShowError></ShowError>
          </VisibleElement>
          
        </div>
      </div>
    )
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  visibilityFilter: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    visibilityFilter: state.visibilityFilter
  }
}

export default connect(mapStateToProps)(App)
