import { combineReducers } from 'redux'
import * as actions from './actions'

export function visibilityFilter(state = actions.VisibilityFilters.SHOW_INITIALIZING, action) {
  switch (action.type) {
    case actions.SET_VISIBILITY_FILTER:
      return action.filter
    default:
      return state
  }
}

export function visibleError(state = '', action) {
  switch (action.type) {
    case actions.SET_VISIBLE_ERROR:
      return action.error
    default:
      return state
  }
}

export const defaultAsyncStatus = {
  isFetching: false,
  error: ''
}

export const defaultSocketStatus = Object.assign({}, defaultAsyncStatus, {
  response: {
    connected: false
  }
})
export function socketStatus(state=defaultSocketStatus, action) {
  switch (action.type) {
    case actions.SOCKET_CONNECT_REQUEST:
    case actions.SOCKET_CONNECT_SUCCESS:
    case actions.SOCKET_CONNECT_ERROR:
        return Object.assign({}, defaultSocketStatus, action.data)
    default:
      return state
  }
}

export const defaultWebLNStatus = Object.assign({}, defaultAsyncStatus, {
  response: {
    connected: false
  }
})
export function webLNStatus(state=defaultWebLNStatus, action) {
  switch (action.type) {
    case actions.WEBLN_PROVIDER_REQUEST:
    case actions.WEBLN_PROVIDER_SUCCESS:
    case actions.WEBLN_PROVIDER_ERROR:
        return Object.assign({}, defaultWebLNStatus, action.data)
    default:
      return state
  }
}

export const defaultChallenge = Object.assign({}, defaultAsyncStatus, {
  response: {
    message: ''
  }
})
export function challenge(state = defaultChallenge, action) {
  switch (action.type) {
    case actions.CHALLENGE_REQUEST:
    case actions.CHALLENGE_SUCCESS:
    case actions.CHALLENGE_ERROR:
      return Object.assign({}, defaultChallenge, action.data)
    default:
      return state
  }
}

export const defaultAuthenticated = Object.assign({}, defaultAsyncStatus, {
  response: {
    id: ''
  }
})
export function authenticated(state = defaultAuthenticated, action) {
  switch (action.type) {
    case actions.AUTHENTICATE_REQUEST:
    case actions.AUTHENTICATE_SUCCESS:
    case actions.AUTHENTICATE_ERROR:
      return Object.assign({}, defaultAuthenticated, action.data)
    default:
      return state
  }
}

export const defaultUsernameAvailable = Object.assign({}, defaultAsyncStatus, {
  response: {
    username: '',
    available: false
  }
})
export function usernameAvailable(state=defaultUsernameAvailable, action) {
  switch (action.type) {
    case actions.USERNAME_AVAILABLE_REQUEST:
    case actions.USERNAME_AVAILABLE_SUCCESS:
    case actions.USERNAME_AVAILABLE_ERROR:
      return Object.assign({}, defaultUsernameAvailable, action.data)
    default:
      return state
  }
}

export const defaultCard = Object.assign({}, defaultAsyncStatus, {
  response: {
    expires_at: 0,
    id: '',
    number: ''
  }
})
export function card(state = defaultCard, action) {
  switch (action.type) {
    case actions.CARD_REQUEST:
    case actions.CARD_SUCCESS:
    case actions.CARD_ERROR:
      return Object.assign({}, defaultCard, action.data)
    default:
      return state
  }
}

export const defaultRecentHistory = Object.assign({}, defaultAsyncStatus, {
  response: {
    payments: []
  }
})
export function recentHistory(state = defaultRecentHistory, action) {
  switch (action.type) {
    case actions.PAYMENT_HISTORY_REQUEST:
    case actions.PAYMENT_HISTORY_SUCCESS:
    case actions.PAYMENT_HISTORY_ERROR:
      return Object.assign({}, defaultRecentHistory, action.data)
    default:
      return state
  }
}

export function historyDetail(state = '', action) {
  switch (action.type) {
    case actions.SET_HISTORY_DETAIL_ID:
      return action.id
    default:
      return state
  }
}

export const defaultPaymentRequestState = {
  id: '',
  card_number: '',
  cancelable_seconds: 0,
  created_at: 0,
  created_by: {
    name: '',
    description: ''
  },
  description: '',
  metadata: '',
  order_id: '',
  satoshi: 0,
  satoshi_received: 0,
  msatoshi_received: 0,
  paid_at: 0,
  payable_seconds: 0,
  payment_request: '',
  identity: '',
  quoted_amount: 0,
  quoted_currency: '',
  quoted_rate: 0,
  r_hash: '',
  rejected_at: 0,
}
export function paymentRequest(state = defaultPaymentRequestState, action) {
  switch (action.type) {
    case actions.SET_PAYMENT_REQUEST:
      return action.response
    default:
      return state
  }
}

const rootReducer = combineReducers({
  authenticated,
  challenge,
  historyDetail,
  paymentRequest,
  recentHistory,
  socketStatus,
  card,
  usernameAvailable,
  visibleError,
  visibilityFilter,
  webLNStatus
})

export default rootReducer