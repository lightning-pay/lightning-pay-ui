import { requestProvider } from 'webln'
import 'cross-fetch/polyfill';
import io from 'socket.io-client'

const API_BASE_URL = process.env.REACT_APP_API_URL + process.env.REACT_APP_API_PATH

export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER'

export const INITIATE_PAYMENT_REQUEST = 'INITIATE_PAYMENT_REQUEST'
export const ACCEPT_PAYMENT_REQUEST = 'ACCEPT_PAYMENT_REQUEST'
export const DECLINE_PAYMENT_REQUEST = 'DECLINE_PAYMENT_REQUEST'

export const WEBLN_PROVIDER_REQUEST = 'WEBLN_PROVIDER_REQUEST'
export const WEBLN_PROVIDER_SUCCESS = 'WEBLN_PROVIDER_SUCCESS'
export const WEBLN_PROVIDER_ERROR = 'WEBLN_PROVIDER_ERROR'

export const CARD_REQUEST = 'CARD_REQUEST'
export const CARD_SUCCESS = 'CARD_SUCCESS'
export const CARD_ERROR = 'CARD_ERROR'

export const CHALLENGE_REQUEST = 'CHALLENGE_REQUEST'
export const CHALLENGE_SUCCESS = 'CHALLENGE_SUCCESS'
export const CHALLENGE_ERROR = 'CHALLENGE_ERROR'

export const AUTHENTICATE_REQUEST = 'AUTHENTICATE_REQUEST'
export const AUTHENTICATE_SUCCESS = 'AUTHENTICATE_SUCCESS'
export const AUTHENTICATE_ERROR = 'AUTHENTICATE_ERROR'

export const PAYMENT_HISTORY_REQUEST = 'PAYMENT_HISTORY_REQUEST'
export const PAYMENT_HISTORY_SUCCESS = 'PAYMENT_HISTORY_SUCCESS'
export const PAYMENT_HISTORY_ERROR = 'PAYMENT_HISTORY_ERROR'

export const REJECT_PAYMENT_REQUEST = 'REJECT_PAYMENT_REQUEST'
export const REJECT_PAYMENT_SUCCESS = 'REJECT_PAYMENT_SUCCESS'
export const REJECT_PAYMENT_ERROR = 'REJECT_PAYMENT_ERROR'

export const SOCKET_CONNECT_REQUEST = 'SOCKET_CONNECT_REQUEST'
export const SOCKET_CONNECT_SUCCESS = 'SOCKET_CONNECT_SUCCESS'
export const SOCKET_CONNECT_ERROR = 'SOCKET_CONNECT_ERROR'

export const WEBLN_SENDPAYMENT_REQUEST = 'WEBLN_SENDPAYMENT_REQUEST'
export const WEBLN_SENDPAYMENT_SUCCESS = 'WEBLN_SENDPAYMENT_SUCCESS'
export const WEBLN_SENDPAYMENT_ERROR = 'WEBLN_SENDPAYMENT_ERROR'

export const USERNAME_AVAILABLE_REQUEST = 'USERNAME_AVAILABLE_REQUEST'
export const USERNAME_AVAILABLE_SUCCESS = 'USERNAME_AVAILABLE_SUCCESS'
export const USERNAME_AVAILABLE_ERROR = 'USERNAME_AVAILABLE_ERROR'

export const SET_AUTHENTICATED = 'SET_AUTHENTICATED'
export const SET_HISTORY_DETAIL_ID = 'SET_HISTORY_DETAIL_ID'
export const SET_VISIBLE_ERROR = 'SET_VISIBLE_ERROR'
export const SET_PAYMENT_REQUEST = 'SET_PAYMENT_REQUEST'


export const VisibilityFilters = {
  SHOW_INITIALIZING: 'SHOW_INITIALIZING',
  SHOW_BASIC_AUTH: 'SHOW_BASIC_AUTH',
  SHOW_CREATE_BASIC_AUTH: 'SHOW_CREATE_BASIC_AUTH',
  SHOW_INIT_PAYMENT_REQUEST: 'SHOW_INIT_PAYMENT_REQUEST',
  SHOW_KEY_AUTH: 'SHOW_KEY_AUTH',
  SHOW_PAYMENT_REQUEST: 'SHOW_PAYMENT_REQUEST',
  SHOW_PAYMENT_PENDING: 'SHOW_PAYMENT_PENDING',
  SHOW_PAYMENT_ACCEPTED: 'SHOW_PAYMENT_ACCEPTED',
  SHOW_PAYMENT_CANCELED: 'SHOW_PAYMENT_CANCELED',
  SHOW_PAYMENT_EXPIRED: 'SHOW_PAYMENT_EXPIRED',
  SHOW_PAYMENT_HISTORY: 'SHOW_PAYMENT_HISTORY',
  SHOW_PAYMENT_HISTORY_DETAIL: 'SHOW_PAYMENT_HISTORY_DETAIL',
  SHOW_ERROR: 'SHOW_ERROR'
}

export function setVisibilityFilter(filter) {
  return { type: SET_VISIBILITY_FILTER, filter }
}

export function setVisibleError(error) {
  return { type: SET_VISIBLE_ERROR, error: error.message || error }
}

export function beginAsyncRequest(type) {
  return {type, data: {isFetching: true, error: ''}}
}

export function endAsyncRequest(type, response) {
  return {type, data: {isFetching: false, error: '', response}}
}

export function endAsyncRequestError(type, error) {
  return {type, data: {isFetching: false, error: error.message || error}}
}

export function endSyncRequest(type, response) {
  return {type, response}
}

export function initSocket() {
  return function(dispatch) {
    if (!window.socket) {
      dispatch(beginAsyncRequest(SOCKET_CONNECT_REQUEST))
      window.socket = io.connect(process.env.REACT_APP_API_URL, {path: process.env.REACT_APP_SOCKETIO_PATH});
      window.socket.on('connect', () => {
        dispatch(endAsyncRequest(SOCKET_CONNECT_SUCCESS, {connected: true}))
      })
      window.socket.on('disconnect', () => {
        dispatch(endAsyncRequest(SOCKET_CONNECT_SUCCESS, {connected: false}))
      })
      window.socket.on('invoice-event', (invoiceEvent) => {
        switch (invoiceEvent.status) {
          case 'open':
            dispatch(endSyncRequest(SET_PAYMENT_REQUEST, invoiceEvent))
            return dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_REQUEST))
          case 'accepted':
            dispatch(endSyncRequest(SET_PAYMENT_REQUEST, invoiceEvent))
            return dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_ACCEPTED))
          case 'canceled':
            dispatch(endSyncRequest(SET_PAYMENT_REQUEST, invoiceEvent))
            return dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_CANCELED))
          default:
            return
        }
      })
    }
  }
}

export function initWebLNProvider() {
  return async function(dispatch) {
    dispatch(setVisibilityFilter(VisibilityFilters.SHOW_INITIALIZING))
    dispatch(beginAsyncRequest(WEBLN_PROVIDER_REQUEST))
    try {
      if(!window.weblnProvider) {
        window.weblnProvider = await requestProvider()
      }
      dispatch(endAsyncRequest(WEBLN_PROVIDER_SUCCESS, {connected: true}))
    } catch (e) {
      dispatch(endAsyncRequestError(WEBLN_PROVIDER_ERROR, e))
    }
  }
}

export function authenticate() {
  return function(dispatch, getState) {
    const state = getState();
    if(!state.authenticated.id){
      if (!window.weblnProvider || !window.weblnProvider.signMessage || window.weblnProvider.signMessage.toString().includes('not implemented')) {
        dispatch(setVisibilityFilter(VisibilityFilters.SHOW_BASIC_AUTH))
      } else {
        dispatch(setVisibilityFilter(VisibilityFilters.SHOW_KEY_AUTH))
      }
    } else {
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
    }
  }
}

export function keyAuthChallenge() {
  return async function(dispatch) {
    dispatch(beginAsyncRequest(CHALLENGE_REQUEST))
    try {
      const response = await fetch(`${API_BASE_URL}/key-auth-challenge`, {
        method: "GET",
        credentials: "include"
      })
      const json = await response.json()

      if (response.status >= 400) {
        let message = `Bad response from server: ${response.status} error`
        if (json.error) {
          message = json.error;
        }
        throw new Error(message)
      }
      
      dispatch(endAsyncRequest(CHALLENGE_SUCCESS, json))
    } catch (e) {
      dispatch(endAsyncRequestError(CHALLENGE_ERROR, e))
      dispatch(setVisibleError(e))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ERROR))
    }
  }
}

export function signKeyChallenge() {
  return async function(dispatch, getState) {
    const state = getState()
    const message = state.challenge.response.message

    dispatch(beginAsyncRequest(AUTHENTICATE_REQUEST))
    try {
      const signature = await window.weblnProvider.signMessage(message)
      const body = {
        message,
        signature: signature.signature
      }
      const response = await fetch(`${API_BASE_URL}/key-auth-signature`, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(body)
      })
      const json = await response.json()

      if (response.status >= 400) {
        let message = `Bad response from server: ${response.status} error`
        if (json.error) {
          message = json.error;
        }
        throw new Error(message)
      }

      dispatch(endAsyncRequest(AUTHENTICATE_REQUEST, json))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
    } catch (e) {
      dispatch(endAsyncRequestError(AUTHENTICATE_ERROR, e))
      dispatch(setVisibleError(e))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ERROR))
    }
  }
}

export function basicAuth(username, password) {
  return async function(dispatch) {
    dispatch(beginAsyncRequest(AUTHENTICATE_REQUEST))
    try {
      const body = {
        username,
        password
      }
      const response = await fetch(`${API_BASE_URL}/basic-auth-credentials`, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(body)
      })
      const json = await response.json()

      if (response.status >= 400) {
        let message = `Bad response from server: ${response.status} error`
        if (json.error) {
          message = json.error;
        }
        throw new Error(message)
      }

      dispatch(endAsyncRequest(AUTHENTICATE_REQUEST, json))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
    } catch (e) {
      dispatch(endAsyncRequestError(AUTHENTICATE_ERROR, e))
      dispatch(setVisibleError(e))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ERROR))
    }
  }
}

export function createBasicAuth(username, password) {
  return async function(dispatch) {
    dispatch(beginAsyncRequest(AUTHENTICATE_REQUEST))
    try {
      const body = {
        username,
        password
      }
      const response = await fetch(`${API_BASE_URL}/new-basic-auth-credentials`, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(body)
      })
      
      const json = await response.json()

      if (response.status >= 400) {
        let message = `Bad response from server: ${response.status} error`
        if (json.error) {
          message = json.error;
        }
        throw new Error(message)
      }

      dispatch(endAsyncRequest(AUTHENTICATE_REQUEST, json))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
    } catch (e) {
      dispatch(endAsyncRequestError(AUTHENTICATE_ERROR, e))
      dispatch(setVisibleError(e))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ERROR))
    }
  }
}

export function checkUsernameAvailable(username) {
  return async function(dispatch) {
    dispatch(beginAsyncRequest(USERNAME_AVAILABLE_REQUEST))
    try {
      const response = await fetch(`${API_BASE_URL}/username-availability/${username}`, {
        method: "GET"
      })
      const json = await response.json()

      if (response.status >= 400) {
        let message = `Bad response from server: ${response.status} error`
        if (json.error) {
          message = json.error;
        }
        throw new Error(message)
      }

      dispatch(endAsyncRequest(USERNAME_AVAILABLE_SUCCESS, json))
    } catch (e) {
      dispatch(endAsyncRequestError(USERNAME_AVAILABLE_ERROR, e))
    }
  }
}

export function requestCard(expirationSeconds = 3600) {
  return async function(dispatch) {
    const start = new Date()

    dispatch(beginAsyncRequest(CARD_REQUEST))
    try {
      const body = {
        expiration_seconds: expirationSeconds
      }
      const response = await fetch(`${API_BASE_URL}/card-request`, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(body)
      })
      const json = await response.json()

      if (response.status >= 400) {
        let message = `Bad response from server: ${response.status} error`
        if (json.error) {
          message = json.error;
        }
        throw new Error(message)
      }

      const finish = new Date()
      const timeRemaining = Math.floor((expirationSeconds * 1000 - (finish - start)));

      setTimeout(() => {
        window.socket.emit('unsubscribe', json.number)
        dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
      }, timeRemaining);


      dispatch(endAsyncRequest(CARD_SUCCESS, json))
      window.socket.emit('subscribe', json.number)
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_INIT_PAYMENT_REQUEST))
    } catch (e) {
      dispatch(endAsyncRequestError(CARD_ERROR, e))
      dispatch(setVisibleError(e))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ERROR))
    }
  }
}

export function fetchPaymentHistory(skip = 0, limit = 20) {
  return async function(dispatch) {
    dispatch(beginAsyncRequest(PAYMENT_HISTORY_REQUEST))
    try {
      const response = await fetch(`${API_BASE_URL}/payments?skip=${skip}&limit=${limit}`, {
        credentials: "include"
      })
      const json = await response.json()

      if (response.status >= 400) {
        let message = `Bad response from server: ${response.status} error`
        if (json.error) {
          message = json.error;
        }
        throw new Error(message)
      }

      dispatch(endAsyncRequest(PAYMENT_HISTORY_SUCCESS, {payments: json}))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
    } catch (e) {
      dispatch(endAsyncRequestError(PAYMENT_HISTORY_ERROR, e))
      dispatch(setVisibleError(e))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ERROR))
    }
  }
}

export function rejectInvoice() {
  return async function(dispatch, getState) {
    const state = getState()
    const invoiceId = state.paymentRequest.id

    dispatch(beginAsyncRequest(REJECT_PAYMENT_REQUEST))

    try {
      const response = await fetch(`${API_BASE_URL}/payment-request/${invoiceId}/rejection`, {
        method: "POST",
        credentials: "include"
      })
      const json = await response.json()

      if (response.status >= 400) {
        let message = `Bad response from server: ${response.status} error`
        if (json.error) {
          message = json.error;
        }
        throw new Error(message)
      }

      dispatch(endAsyncRequest(REJECT_PAYMENT_SUCCESS, json))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
    } catch (e) {
      dispatch(endAsyncRequestError(REJECT_PAYMENT_ERROR, e))
      dispatch(setVisibleError(e))
      dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ERROR))
    }
  }
}

export function sendWebLNPayment() {
  return function(dispatch, getState) {
    const state = getState()
    const paymentRequestString = state.paymentRequest.payment_request

    window.weblnProvider.sendPayment(paymentRequestString)
  }
}

export function cancelInitPayment() {
  return function(dispatch) {
    dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
  }
}

export function viewHistoryDetail(id) {
  return async function(dispatch) {
    dispatch({ type: SET_HISTORY_DETAIL_ID, id })
    dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY_DETAIL))
  }
}

export function closeHistoryDetail() {
  return async function(dispatch) {
    dispatch(setVisibilityFilter(VisibilityFilters.SHOW_PAYMENT_HISTORY))
  }
}