import configureMockStore from 'redux-mock-store'
import {EventEmitter} from 'events'
import io from 'socket.io-client'
import sinon from 'sinon'
import thunk from 'redux-thunk'
import * as webln from 'webln'
import * as actions from './actions'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

const stubs = {};

describe('synchronous actions', () => {
    it('setVisibilityFilter should return a properly formatted object', () => {
        const object = actions.setVisibilityFilter('test');
        expect(object).toEqual({
            type: actions.SET_VISIBILITY_FILTER,
            filter: 'test'
        })
    })
    
    it('beginAsyncRequest should return a properly formatted object', () => {
        const object = actions.beginAsyncRequest('test');
        expect(object).toEqual({
            type: 'test',
            data: {
                isFetching: true,
                error: ''
            }
        })
    })
    
    it('endAsyncRequest should return a properly formatted object', () => {
        const object = actions.endAsyncRequest('test', {test: 'value'});
        expect(object).toEqual({
            type: 'test',
            data: {
                isFetching: false,
                error: '',
                response: {
                    test: 'value'
                }
            }            
        })
    })
    
    it('endAsyncRequestError should return a properly formatted object when given an error object', () => {
        const object = actions.endAsyncRequestError('test', new Error('test error'));
        expect(object).toEqual({
            type: 'test',
            data: {
                isFetching: false,
                error: 'test error'
            }
        })
    })
    
    it('endAsyncRequestError should return a properly formatted object when given an error string', () => {
        const object = actions.endAsyncRequestError('test', 'test error');
        expect(object).toEqual({
            type: 'test',
            data: {
                isFetching: false,
                error: 'test error'
            }
        })
    })
})

describe('init socket', () => {
    let fakeSocketEmitter;

    beforeEach(() => {
        fakeSocketEmitter = new EventEmitter()
        stubs.socketConnect = sinon.stub(io, 'connect').returns(fakeSocketEmitter)
    })
  
    afterEach(() => {
        stubs.socketConnect.restore()
    })
    
    it('should call connect() with correct args', () => {
        const store = mockStore({})
        store.dispatch(actions.initSocket());
        expect(stubs.socketConnect.called).toEqual(true)
    })

    it('should dispatch actions on socket connect', () => {
        window.socket = undefined
        const store = mockStore({})
        store.dispatch(actions.initSocket());
        expect(stubs.socketConnect.called).toEqual(true)
        const expectedActions = [ 
            { type: 'SOCKET_CONNECT_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'SOCKET_CONNECT_SUCCESS', data: {isFetching: false, error: '', response: { connected: true } }}
        ]

        fakeSocketEmitter.emit('connect')
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should dispatch actions on socket disconnect', () => {
        window.socket = undefined
        const store = mockStore({})
        store.dispatch(actions.initSocket());
        expect(stubs.socketConnect.called).toEqual(true)
        const expectedActions = [ 
            { type: 'SOCKET_CONNECT_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'SOCKET_CONNECT_SUCCESS', data: {isFetching: false, error: '', response: { connected: false } }}
        ]

        fakeSocketEmitter.emit('disconnect')
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should dispatch actions on invoice open', () => {
        window.socket = undefined
        const store = mockStore({})
        store.dispatch(actions.initSocket());
        expect(stubs.socketConnect.called).toEqual(true)
        const expectedActions = [
            { type: 'SOCKET_CONNECT_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'SET_PAYMENT_REQUEST', response: { status: 'open' } },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_REQUEST' }
        ]
        
        fakeSocketEmitter.emit('invoice-event', {status: 'open'})
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should dispatch actions on invoice accepted', () => {
        window.socket = undefined
        const store = mockStore({})
        store.dispatch(actions.initSocket());
        expect(stubs.socketConnect.called).toEqual(true)
        const expectedActions = [
            { type: 'SOCKET_CONNECT_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'SET_PAYMENT_REQUEST', response: { status: 'accepted' } },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_ACCEPTED' }
        ]
        
        fakeSocketEmitter.emit('invoice-event', {status: 'accepted'})
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should dispatch actions on invoice canceled', () => {
        window.socket = undefined
        const store = mockStore({})
        store.dispatch(actions.initSocket());
        expect(stubs.socketConnect.called).toEqual(true)
        const expectedActions = [
            { type: 'SOCKET_CONNECT_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'SET_PAYMENT_REQUEST', response: { status: 'canceled' } },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_CANCELED' }
        ]
        
        fakeSocketEmitter.emit('invoice-event', {status: 'canceled'})
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should not dispatch actions on invoice settled', () => {
        window.socket = undefined
        const store = mockStore({})
        store.dispatch(actions.initSocket());
        expect(stubs.socketConnect.called).toEqual(true)
        const expectedActions = [
            { type: 'SOCKET_CONNECT_REQUEST', data: {isFetching: true, error: '' }}
        ]
        
        fakeSocketEmitter.emit('invoice-event', {status: 'settled'})
        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('initWebLNProvider', () => {

    afterEach(() => {
        stubs.requestProvider.restore()
    })

    it('should not skip if provider unset', async () => {
        stubs.requestProvider = sinon.stub(webln, 'requestProvider').resolves(() => {
            return {
                signMessage: () => {}
            }
        })
        const store = mockStore({})
        await store.dispatch(actions.initWebLNProvider());
        const expectedActions = [
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_INITIALIZING' },
            { type: 'WEBLN_PROVIDER_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'WEBLN_PROVIDER_SUCCESS', data: {isFetching: false,  error: '', response: { connected: true } }}
        ]

        expect(stubs.requestProvider.called).toEqual(true)
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should add error to state', async () => {
        window.weblnProvider = undefined
        stubs.requestProvider = sinon.stub(webln, 'requestProvider').rejects(new Error('test error'))
        const store = mockStore({})
        await store.dispatch(actions.initWebLNProvider());
        const expectedActions = [
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_INITIALIZING' },
            { type: 'WEBLN_PROVIDER_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'WEBLN_PROVIDER_ERROR', data: {isFetching: false,  error: 'test error'}}
        ]

        expect(stubs.requestProvider.called).toEqual(true)
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should skip if provider already defined', async () => {
        stubs.requestProvider = sinon.stub(webln, 'requestProvider').resolves(() => {
            return {
                signMessage: () => {}
            }
        })
        window.weblnProvider = {signMessage: () => {}}
        const store = mockStore({})
        await store.dispatch(actions.initWebLNProvider());
        const expectedActions = [
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_INITIALIZING' },
            { type: 'WEBLN_PROVIDER_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'WEBLN_PROVIDER_SUCCESS', data: {isFetching: false,  error: '', response: { connected: true } }}
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('authenticate', () => {
    it('should show basic auth when key auth not available and not authenticated', () => {
        window.weblnProvider = {
            signMessage: () => {
                throw new Error('not implemented')
            }
        }
        const store = mockStore({authenticated: {id: ''}})
        store.dispatch(actions.authenticate());

        const expectedActions = [
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_BASIC_AUTH' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
    
    it('should show key auth if possible and not authenticated', () => {
        window.weblnProvider = {
            signMessage: () => {}
        }
        const store = mockStore({authenticated: {id: ''}})
        store.dispatch(actions.authenticate());

        const expectedActions = [
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_KEY_AUTH' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })



    it('should show payment history when already authenticated', () => {
        const store = mockStore({authenticated: {id: '12345'}})
        store.dispatch(actions.authenticate());

        const expectedActions = [
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('key auth', () => {
    beforeEach(() => {
        fetch.resetMocks()
    })

    it('should request a key auth challenge and handle response error', async () => {
        fetch.mockResponse(JSON.stringify({error: 'Could not create auth challenge. Please try again'}), {status: 500})
        const store = mockStore({})
        await store.dispatch(actions.keyAuthChallenge());

        const expectedActions = [
            { type: 'CHALLENGE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'CHALLENGE_ERROR', data: {isFetching: false, error: 'Could not create auth challenge. Please try again' }},
            { type: 'SET_VISIBLE_ERROR', error: 'Could not create auth challenge. Please try again' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_ERROR' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should request a key auth challenge', async () => {
        fetch.mockResponse(JSON.stringify({message: 'Sign this message to authenticate. 67b064120c951e280d832693ca4e1411'}), {status: 201})
        const store = mockStore({})
        await store.dispatch(actions.keyAuthChallenge());

        const expectedActions = [
            { type: 'CHALLENGE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'CHALLENGE_SUCCESS', data: {isFetching: false, error: '', response:
            { message: 'Sign this message to authenticate. 67b064120c951e280d832693ca4e1411' } }}
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should not authenticate with an invalid signature', async () => {
        window.weblnProvider.signMessage = (message) => {
            return {
                message,
                signature: 'dsfgsfgsdfgd'
            }
        }
        fetch.mockResponse(JSON.stringify({error: 'Error parsing challenge signature'}), {status: 403})
        const store = mockStore({challenge: {response: {message: 'Sign this message to authenticate. 67b064120c951e280d832693ca4e1411'}}})
        await store.dispatch(actions.signKeyChallenge());

        const expectedActions = [
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'AUTHENTICATE_ERROR', data: {isFetching: false, error: 'Error parsing challenge signature' }},
            { type: 'SET_VISIBLE_ERROR', error: 'Error parsing challenge signature' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_ERROR' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should authenticate with a valid signature', async () => {
        window.weblnProvider.signMessage = (message) => {
            return {
                message,
                signature: 'dsfgsfgsdfgd'
            }
        }
        fetch.mockResponse(JSON.stringify({id: '1234567890'}), {status: 200})
        const store = mockStore({challenge: {response: {message: 'Sign this message to authenticate. 67b064120c951e280d832693ca4e1411'}}})
        await store.dispatch(actions.signKeyChallenge());

        const expectedActions = [
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: false, error: '', response: { id: '1234567890' } }},
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('basic auth', () => {
    beforeEach(() => {
        fetch.resetMocks()
    })
    
    it('should authenticate valid credentials', async () => {
        window.weblnProvider = undefined
        fetch.mockResponse(JSON.stringify({id: '12345'}), {status: 200})
        const store = mockStore({})
        await store.dispatch(actions.basicAuth('fakeUsername', 'fakePassword'));

        const expectedActions = [
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: false, error: '', response: { id: '12345' } }},
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should not authenticate invalid credentials', async () => {
        window.weblnProvider = undefined
        fetch.mockResponse(JSON.stringify({error: 'Could not authenticate user. Please try again'}), {status: 403})
        const store = mockStore({})
        await store.dispatch(actions.basicAuth('fakeUsername', 'fakePassword'));

        const expectedActions = [
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'AUTHENTICATE_ERROR', data: {isFetching: false, error: 'Could not authenticate user. Please try again' }},
            { type: 'SET_VISIBLE_ERROR', error: 'Could not authenticate user. Please try again' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_ERROR' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should allow creating new credentials', async () => {
        window.weblnProvider = undefined
        fetch.mockResponse(JSON.stringify({id: '12345'}), {status: 201})
        const store = mockStore({})
        await store.dispatch(actions.createBasicAuth('fakeUsername', 'fakePassword'));

        const expectedActions = [
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: false, error: '', response: { id: '12345' } }},
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should handle errors creating new crednetials', async () => {
        window.weblnProvider = undefined
        fetch.mockResponse(JSON.stringify({error: 'Could not create user. Please try again'}), {status: 500})
        const store = mockStore({})
        await store.dispatch(actions.createBasicAuth('fakeUsername', 'fakePassword'));

        const expectedActions = [
            { type: 'AUTHENTICATE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'AUTHENTICATE_ERROR', data: {isFetching: false, error: 'Could not create user. Please try again' }},
            { type: 'SET_VISIBLE_ERROR', error: 'Could not create user. Please try again' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_ERROR' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should allow checking username availability', async () => {
        fetch.mockResponse(JSON.stringify({username: 'fakeUsername', available: true}), {status: 200})
        const store = mockStore({})
        await store.dispatch(actions.checkUsernameAvailable('fakeUsername'));

        const expectedActions = [
            { type: 'USERNAME_AVAILABLE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'USERNAME_AVAILABLE_SUCCESS', data: {isFetching: false, error: '', response: { username: 'fakeUsername', available: true } }}
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should handle errors checking username availability', async () => {
        fetch.mockResponse(JSON.stringify({error: 'Temporary system error. Please try your request again.'}), {status: 500})
        const store = mockStore({})
        await store.dispatch(actions.checkUsernameAvailable('fakeUsername'));

        const expectedActions = [
            { type: 'USERNAME_AVAILABLE_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'USERNAME_AVAILABLE_ERROR', data: {isFetching: false, error: 'Temporary system error. Please try your request again.' }}
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('requesting a card', () => {
    function secondsFromNow(seconds = 120) {
        const t = new Date();
        t.setSeconds(t.getSeconds() + seconds);
        return t;
    }

    beforeEach(() => {
        fetch.resetMocks()
        window.socket = new EventEmitter()
    })

    it('should provide a card for authenticated users', async () => {
        const expires_at = secondsFromNow(1)
        fetch.mockResponse(JSON.stringify({expires_at, identity: '12345', number: '1234567890123456'}), {status: 201})
        const store = mockStore({})
        await store.dispatch(actions.requestCard(1));

        // force wait
        await new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 1100)
        })

        const expectedActions = [
            { type: 'CARD_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'CARD_SUCCESS', data: {isFetching: false, error: '', response: {
                expires_at: expires_at.toISOString(),
                identity: '12345',
                number: '1234567890123456' }
            }},
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_INIT_PAYMENT_REQUEST' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should not provide a card to unauthenticated users', async () => {
        fetch.mockResponse(JSON.stringify({error: 'Not authenticated'}), {status: 403})
        const store = mockStore({})
        await store.dispatch(actions.requestCard(1));

        const expectedActions = [
            { type: 'CARD_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'CARD_ERROR', data: {isFetching: false, error: 'Not authenticated' }},
            { type: 'SET_VISIBLE_ERROR', error: 'Not authenticated' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_ERROR' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('fetching payment history', () => {
    beforeEach(() => {
        fetch.resetMocks()
    })

    it('should fetch history for authenticated users', async () => {
        fetch.mockResponse(JSON.stringify([]), {status: 200})
        const store = mockStore({})
        await store.dispatch(actions.fetchPaymentHistory());

        const expectedActions = [
            { type: 'PAYMENT_HISTORY_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'PAYMENT_HISTORY_SUCCESS', data: {isFetching: false, error: '', response: { payments: [] } }},
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should handle errors', async () => {
        fetch.mockResponse(JSON.stringify({error: 'Not authenticated'}), {status: 403})
        const store = mockStore({})
        await store.dispatch(actions.fetchPaymentHistory());

        const expectedActions = [
            { type: 'PAYMENT_HISTORY_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'PAYMENT_HISTORY_ERROR', data: {isFetching: false, error: 'Not authenticated' }},
            { type: 'SET_VISIBLE_ERROR', error: 'Not authenticated' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_ERROR' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('rejecting an invoice', () => {
    beforeEach(() => {
        fetch.resetMocks()
    })

    it('should reject for authenticated users', async () => {
        fetch.mockResponse(JSON.stringify({}), {status: 200})
        const store = mockStore({paymentRequest: {id: 'sdfsdfs'}})
        await store.dispatch(actions.rejectInvoice());

        const expectedActions = [
            { type: 'REJECT_PAYMENT_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'REJECT_PAYMENT_SUCCESS', data: {isFetching: false, error: '', response: {} }},
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should handle errors', async () => {
        fetch.mockResponse(JSON.stringify({error: 'Not authenticated'}), {status: 403})
        const store = mockStore({paymentRequest: {id: 'sdfsdfs'}})
        await store.dispatch(actions.rejectInvoice());

        const expectedActions = [
            { type: 'REJECT_PAYMENT_REQUEST', data: {isFetching: true, error: '' }},
            { type: 'REJECT_PAYMENT_ERROR', data: {isFetching: false, error: 'Not authenticated' }},
            { type: 'SET_VISIBLE_ERROR', error: 'Not authenticated' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_ERROR' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('paying with webln', () => {
    it('should send payment', async () => {
        window.weblnProvider = {sendPayment: sinon.spy()}
        const store = mockStore({paymentRequest: {payment_request: 'sdfsdfs'}})
        store.dispatch(actions.sendWebLNPayment())

        expect(window.weblnProvider.sendPayment.called).toEqual(true)

        const expectedActions = []

        expect(store.getActions()).toEqual(expectedActions)
    })
})

describe('nav actions', () => {
    beforeAll(() => {
        window.weblnProvider = undefined
    })

    it('should show payment history when canceling init payment', () => {
        const store = mockStore({})
        store.dispatch(actions.cancelInitPayment())
        const expectedActions = [
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should show payment history detail', () => {
        const store = mockStore({})
        store.dispatch(actions.viewHistoryDetail('12345'))
        const expectedActions = [
            { type: 'SET_HISTORY_DETAIL_ID', id: '12345' },
            { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_PAYMENT_HISTORY_DETAIL' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })

    it('should show payment history when closing history detail', () => {
        const store = mockStore({})
        store.dispatch(actions.closeHistoryDetail())
        const expectedActions = [
            { type: 'SET_VISIBILITY_FILTER',  filter: 'SHOW_PAYMENT_HISTORY' }
        ]

        expect(store.getActions()).toEqual(expectedActions)
    })
})