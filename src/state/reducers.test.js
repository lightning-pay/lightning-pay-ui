import {
    visibilityFilter,
    visibleError,
    socketStatus,
    webLNStatus,
    challenge,
    authenticated,
    usernameAvailable,
    card,
    recentHistory,
    historyDetail,
    defaultPaymentRequestState,
    paymentRequest
} from './reducers'

describe('simple setters', () => {
    it('should provide default visibilityFilter state', () => {
        const state = visibilityFilter(undefined, {})
        expect(state).toEqual('SHOW_INITIALIZING')
    })

    it('should update visibilityFilter state', () => {
        const state = visibilityFilter({}, {
            type: 'SET_VISIBILITY_FILTER',
            filter: 'SHOW_SOMETHING'
        })
        expect(state).toEqual('SHOW_SOMETHING')
    })

    it('should provide default visibleError state', () => {
        const state = visibleError(undefined, {})
        expect(state).toEqual('')
    })

    it('should update visibleError state', () => {
        const state = visibleError({}, {
            type: 'SET_VISIBLE_ERROR',
            error: 'some error'
        })
        expect(state).toEqual('some error')
    })

    it('should provide default paymentRequest state', () => {
        const state = paymentRequest(undefined, {})
        expect(state).toEqual(defaultPaymentRequestState)
    })

    it('should update paymentRequest state', () => {
        const response = { 
           card_number: '2142838675711575',
           cancelable_seconds: 300,
           created_at: '2019-09-12T17:52:25.532Z',
           created_by:
            { name: 'Satterfield Inc',
              description: 'Customer-focused bifurcated alliance' },
           description: 'Order placed via test script',
           expires_at: '2019-09-12T17:52:25.532Z',
           metadata: '{"some":"metadata"}',
           order_id: '1234567890',
           satoshi: 98,
           payable_seconds: 30,
           payment_request: 'lnbc980n1pwh4pdmpp5aj0h3waf0lanq0qdu6c2pszzmhf29pknlmhwwgqwdfgxhruqjaasdp52dshgar9wfnxjetvvssyjmnryp8hyer9wgsrzv3nxs6nvdec8ycqcqzpgxqzpmdhjl0saxw6skvkfksgemp5nup2xlsz0qj63vcwjgy6ekpadlryf49up0avpngslrqqfq76zc7ydv0uyxv3yafrlf3hlrwkq2snf68aqpslkewt',
           identity: '02f35dd00ddbef0d683f3f13c1220ab5dffa0e697aa4c2658774fd5e02e046b878',
           quoted_amount: '0.01',
           quoted_currency: 'USD',
           quoted_rate: 10304.118997002262,
           r_hash: '7J94u6l/+zA8DeawoMBC3dKihtP+7ucgDmpQa4+Al3s=',
           refunds: [],
           status: 'open',
           id: '5d7a85bbfcafe70018f84663'
        }

        const state = paymentRequest({}, {
            type: 'SET_PAYMENT_REQUEST',
            response
        })

        expect(state).toEqual(response)
    })

    it('should provide default historyDetail state', () => {
        const state = historyDetail(undefined, {})
        expect(state).toEqual('')
    })

    it('should update historyDetail state', () => {
        const state = historyDetail({}, {
            type: 'SET_HISTORY_DETAIL_ID',
            id: '5432'
        })
        expect(state).toEqual('5432')
    })
})

describe('async lifecycle reducers', () => {
    for (const type of ['_REQUEST', '_SUCCESS', '_ERROR']) {
        it(`should set SOCKET_CONNECT${type} state`, () => {
            const data = {
                isFetching: true,
                error: 'some error',
                response: {
                    connected: true
                }
            }
            const state = socketStatus({}, Object.assign({
                type: `SOCKET_CONNECT${type}`
            }, {data}))

            expect(state).toEqual(data)
        })

        it(`should set WEBLN_PROVIDER${type} state`, () => {
            const data = {
                isFetching: true,
                error: 'some error',
                response: {
                    connected: true
                }
            }
            const state = webLNStatus({}, Object.assign({
                type: `WEBLN_PROVIDER${type}`
            }, {data}))

            expect(state).toEqual(data)
        })

        it(`should set CHALLENGE${type} state`, () => {
            const data = {
                isFetching: true,
                error: 'some error',
                response: {
                    message: '12345'
                }
            }
            const state = challenge({}, Object.assign({
                type: `CHALLENGE${type}`
            }, {data}))

            expect(state).toEqual(data)
        })

        it(`should set AUTHENTICATE${type} state`, () => {
            const data = {
                isFetching: true,
                error: 'some error',
                response: {
                    id: '12345'
                }
            }
            const state = authenticated({}, Object.assign({
                type: `AUTHENTICATE${type}`
            }, {data}))

            expect(state).toEqual(data)
        })

        it(`should set USERNAME_AVAILABLE${type} state`, () => {
            const data = {
                isFetching: true,
                error: 'some error',
                response: {
                    username: 'bob',
                    available: true
                }
            }
            const state = usernameAvailable({}, Object.assign({
                type: `USERNAME_AVAILABLE${type}`
            }, {data}))

            expect(state).toEqual(data)
        })

        it(`should set CARD${type} state`, () => {
            const data = {
                isFetching: true,
                error: 'some error',
                response: {
                    expires_at: 12345,
                    id: '67890',
                    number: '0987654321'
                }
            }
            const state = card({}, Object.assign({
                type: `CARD${type}`
            }, {data}))

            expect(state).toEqual(data)
        })

        it(`should set PAYMENT_HISTORY${type} state`, () => {
            const data = {
                isFetching: true,
                error: 'some error',
                response: {
                    payments: []
                }
            }
            const state = recentHistory({}, Object.assign({
                type: `PAYMENT_HISTORY${type}`
            }, {data}))

            expect(state).toEqual(data)
        })
    }
})