import React, {Component} from 'react';
import Card from 'react-bootstrap/Card'
import { connect } from 'react-redux'

import * as actions from '../state/actions'

import './Initializing.css'

export class Initializing extends Component {
  async componentDidMount() {
    this.props.dispatch(actions.initSocket())
    await this.props.dispatch(actions.initWebLNProvider())
    this.props.dispatch(actions.authenticate())
  }

  render(){
    return (
      <Card className="text-center">
          <Card.Header>Initializing...</Card.Header>
          <Card.Body>
            <span style={{fontSize: "6em", color: "SlateGray"}}>
                  <i className="fas fa-dice-d20 initializing"></i>
            </span>
          </Card.Body>
      </Card>
    )
  }
}

export default connect()(Initializing)
