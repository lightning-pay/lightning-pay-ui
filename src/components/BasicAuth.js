import React, {Component} from 'react'
import { connect } from 'react-redux'

import {basicAuth, setVisibilityFilter, VisibilityFilters} from '../state/actions'

import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'

export class BasicAuth extends Component {
  constructor(props) {
    super(props)
    this.createAccount = this.createAccount.bind(this)
    this.submitForm = this.submitForm.bind(this)
  }

  createAccount() {
    this.props.dispatch(setVisibilityFilter(VisibilityFilters.SHOW_CREATE_BASIC_AUTH))
  }

  submitForm(event) {
    event.preventDefault();
    this.props.dispatch(basicAuth(this.refs.username.value, this.refs.password.value))
  }

  render() {
    return (
      <Card className="text-center">
        <Card.Header>Sign In</Card.Header>
        <Card.Body>
          <div style={{textAlign: "left"}}>
            <Form method="POST" action="/api/basic-auth-credentials">
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Username</Form.Label>
                <Form.Control type="text" ref="username" placeholder="Enter username" autoComplete="username"/>
              </Form.Group>
  
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" ref="password" placeholder="Password" autoComplete="current-password"/>
              </Form.Group>
              <div style={{textAlign: "center"}}>
                <Button id="submitBasicAuth" variant="primary" size="lg" type="submit" style={{margin: "10px"}} onClick={this.submitForm}>
                  Continue
                </Button>
                <br/>
                <Button id="createAccountLink" variant="link" onClick={this.createAccount}>Create an account</Button>
              </div>
            </Form>
          </div>
        </Card.Body>
      </Card>
    )
  }
}

export default connect()(BasicAuth)
