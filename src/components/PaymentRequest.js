import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import QRCode from 'qrcode.react'
import * as actions from '../state/actions'

class PaymentRequest extends Component {
  constructor(props) {
    super(props)
    this.state = {secondsRemaining: props.expiresSeconds}
    this.payInvoice = this.payInvoice.bind(this)
    this.rejectInvoice = this.rejectInvoice.bind(this)
  }

  tick() {
    this.setState(prevState => ({
      secondsRemaining: prevState.secondsRemaining - 1
    }))
    if (this.state.secondsRemaining <= 0) {
      this.props.dispatch(actions.setVisibilityFilter(actions.VisibilityFilters.SHOW_PAYMENT_EXPIRED))
    }
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000)
    this.payInvoice();
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  payInvoice() {
    if(window.webln) {
      this.props.dispatch(actions.sendWebLNPayment())
    }
  }

  rejectInvoice() {
    this.props.dispatch(actions.rejectInvoice(this.props.id))
  }

  render() {
    const paymentUrl = `lightning:${this.props.paymentRequest}`
    let displayDetails = "block"
    if (window.webln) {
      displayDetails = "none"
    }
    return (
        <Card className="text-center">
          <Card.Body>
            <Card.Text>
              {this.props.merchantName}<br/>
              Order #: {this.props.orderId}
            </Card.Text>
            <Card.Text>
              {this.props.description}<br/>
            </Card.Text>
            <Card.Text>
              Amount: {this.props.fiatAmount} {this.props.fiatCurrency}<br/>
              Amount: {this.props.cryptoAmount} {this.props.cryptoCurrency}<br/>
              Rate: 1 {this.props.cryptoCurrency} = {this.props.conversionRate} {this.props.fiatCurrency}
            </Card.Text>
            <Card.Text>
              Expires in {this.state.secondsRemaining} seconds...
            </Card.Text>
            <div style={{textAlign: "center", display: displayDetails}}>
              <QRCode
                value={this.props.paymentRequest}
                size={240}
                bgColor="#ffffff"
                fgColor="#333333"
                level="M"
                includeMargin={false}
                renderAs="svg"
              />
              <br/>
              <br/>
              <Button
                variant="primary"
                size="lg"
                href={paymentUrl}
                target="_blank"
              >Open in Wallet</Button>
            </div>
            <div style={{textAlign: "center"}}>
              <br/>
              <Button
                variant="outline-danger"
                size="lg"
                onClick={this.rejectInvoice}
              >
                Reject
              </Button>
            </div>
        </Card.Body>
      </Card>
    )
  }
}

PaymentRequest.propTypes = {
  id: PropTypes.string.isRequired,
  conversionRate: PropTypes.string.isRequired,
  cryptoAmount: PropTypes.string.isRequired,
  cryptoCurrency: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  expiresSeconds: PropTypes.number.isRequired,
  fiatAmount: PropTypes.string.isRequired,
  fiatCurrency: PropTypes.string.isRequired,
  merchantName: PropTypes.string.isRequired,
  orderId: PropTypes.string.isRequired,
  paymentRequest: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  console.log('PaymentRequest State', state.paymentRequest)
  return {
    id: state.paymentRequest.id,
    conversionRate: state.paymentRequest.quoted_rate.toFixed(2),
    cryptoAmount: (state.paymentRequest.satoshi / 1e8).toFixed(8),
    cryptoCurrency: 'BTC',
    description: state.paymentRequest.description,
    fiatAmount: state.paymentRequest.quoted_amount,
    fiatCurrency: state.paymentRequest.quoted_currency,
    merchantName: state.paymentRequest.created_by.name,
    orderId: state.paymentRequest.order_id,
    paymentRequest: state.paymentRequest.payment_request,
    expiresSeconds: 30
  }
}

export default connect(mapStateToProps)(PaymentRequest)
