import React from 'react';
import ReactDOM from 'react-dom'

import sinon from 'sinon'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import { Provider } from 'react-redux'

import KeyAuthConnected from './KeyAuth'
import * as actions from '../state/actions'

const mockStore = configureStore([thunk])

const stubs = {};

beforeAll(() => {
  stubs.keyAuthChallenge = sinon.stub(actions, 'keyAuthChallenge').returns(() => {
    return async function(dispatch) {}
  })
  stubs.signKeyChallenge = sinon.stub(actions, 'signKeyChallenge').returns(() => {
    return async function(dispatch) {}
  })
})

afterAll(() => {
  stubs.keyAuthChallenge.restore()
  stubs.signKeyChallenge.restore()
})

it('renders', async () => {
  const state = {
    authenticated: {id: ''},
    visibilityFilter: actions.VisibilityFilters.SHOW_KEY_AUTH
  }
  const store = mockStore(state);
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <KeyAuthConnected/>
    </Provider>,
    div
  );

  // force wait
  await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 100)
  })
  expect(stubs.keyAuthChallenge.called).toEqual(true)
  expect(stubs.signKeyChallenge.called).toEqual(true)
});