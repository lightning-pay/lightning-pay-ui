import { connect } from 'react-redux'
import VisibilityWrapper from '../containers/VisibilityWrapper'

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.active === true
  }
}

const VisibleElement = connect(
  mapStateToProps
)(VisibilityWrapper)

export default VisibleElement