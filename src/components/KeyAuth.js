import React, {Component} from 'react';
import Card from 'react-bootstrap/Card'

import { connect } from 'react-redux'

import * as actions from '../state/actions'

export class KeyAuth extends Component {
  async componentDidMount() {
    await this.props.dispatch(actions.keyAuthChallenge())
    await this.props.dispatch(actions.signKeyChallenge())
  }

  render() {
    return (
      <Card className="text-center">
          <Card.Header>Awaiting Signature</Card.Header>
          <Card.Body>
            <span style={{fontSize: "6em", color: "SlateGray"}}>
                  <i className="fas fa-file-signature"></i>
            </span>
            <Card.Text>
                Please sign the request to continue
            </Card.Text>
          </Card.Body>
      </Card>
    )
  }
}

export default connect()(KeyAuth)
