import React from 'react'
import PropTypes from 'prop-types'

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Navbar from 'react-bootstrap/Navbar';
import VisibleElement from './VisibleElement';

import * as actions from '../state/actions'

function Navigation({initPayment, visibilityFilter}) {
  return (
    <Navbar variant="dark" className="justify-content-between bg-midnight" sticky="top">
      <Navbar.Brand href="https://lightningpay.io">
        <img src="logo-inverted-32.png" width="30" height="30" className="d-inline-block align-top" alt=""/>
      </Navbar.Brand>

      <Form inline>
        <VisibleElement
        active={visibilityFilter === actions.VisibilityFilters.SHOW_PAYMENT_HISTORY || visibilityFilter === actions.VisibilityFilters.SHOW_PAYMENT_HISTORY_DETAIL || visibilityFilter === actions.VisibilityFilters.SHOW_INTRODUCTION}>
          <Button variant="outline-light" onClick={initPayment}>
            Pay
          </Button>
        </VisibleElement>
      </Form>
    </Navbar>
  )
}

Navigation.propTypes = {
  initPayment: PropTypes.func.isRequired,
  visibilityFilter: PropTypes.string.isRequired
}

export default Navigation
