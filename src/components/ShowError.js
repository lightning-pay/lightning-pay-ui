import React, {Component} from 'react';
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import * as actions from '../state/actions'

export class ShowError extends Component {
  constructor(props) {
    super(props)
    this.continue = this.continue.bind(this)
  }

  continue() {
    this.props.dispatch(actions.setVisibilityFilter(actions.VisibilityFilters.SHOW_PAYMENT_HISTORY))
  }

  render() {
    return (
      <Card>
        <Card.Header>Error</Card.Header>
        <Card.Body>
          <span style={{fontSize: "6em", color: "SlateGray"}}>
              <i className="fas fa-bomb"></i>
          </span>
          <Card.Text style={{textAlign: "center"}}>
            Error: {this.props.errorMessage}
          </Card.Text>
          <div style={{textAlign: "center"}}>
          <Button
              variant="primary"
              size="lg"
              onClick={this.continue}
          >Continue</Button>
          </div>
        </Card.Body>
      </Card>
    );
    }
}

ShowError.propTypes = {
    errorMessage: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    errorMessage: state.visibleError
  }
}

export default connect(mapStateToProps)(ShowError)
