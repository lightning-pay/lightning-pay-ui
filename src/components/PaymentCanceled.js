import React, {Component} from 'react';
import { connect } from 'react-redux'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'

import * as actions from '../state/actions'

export class PaymentCanceled extends Component {
  constructor(props) {
    super(props)
    this.continue = this.continue.bind(this)
  }
  
  continue() {
    this.props.dispatch(actions.setVisibilityFilter(actions.VisibilityFilters.SHOW_PAYMENT_HISTORY))
  }

  render() {
    return (
      <Card className="text-center">
          <Card.Body>
              <span style={{fontSize: "6em", color: "Gold"}}>
                <i className="fas fa-exclamation-triangle"></i>
              </span>
              <Card.Text>
                Payment Canceled
              </Card.Text>
              <div style={{textAlign: "center"}}>
              <Button
                  variant="primary"
                  size="lg"
                  onClick={this.continue}
              >Continue</Button>
              </div>
          </Card.Body>
      </Card>
    )
  }
}

export default connect()(PaymentCanceled)