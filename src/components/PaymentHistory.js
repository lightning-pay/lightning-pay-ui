import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import VisibleElement from './VisibleElement';

import * as actions from '../state/actions'

class PaymentHistory extends Component {
  constructor(props) {
    super(props)
    this.viewHistoryDetail = this.viewHistoryDetail.bind(this)
  }

  componentDidMount() {
    this.props.dispatch(actions.fetchPaymentHistory())
  }

  viewHistoryDetail(id) {
    this.props.dispatch(actions.viewHistoryDetail(id))
  }

  render() {
    return (
      <div>
        <VisibleElement active={this.props.isFetching === false && this.props.history.length === 0}>
          <Card>
            <Card.Header>Welcome to Lightning Pay!</Card.Header>
            <Card.Body>
              <div style={{textAlign: "center"}}>
                <img src="logo-512.png" width="260" alt="lightning pay logo"/>
              </div>
              <Card.Text style={{textAlign: "center"}}>
                <i>Instant Bitcoin Payments</i>
              </Card.Text>
              <Card.Text style={{textAlign: "center"}}>
                Click <b>Pay</b> to make a payment
              </Card.Text>
            </Card.Body>
          </Card>
        </VisibleElement>

        <VisibleElement active={this.props.history.length > 0}>
          <Card>
            <Card.Header style={{textAlign: "center"}}>Payment History</Card.Header>
            <ListGroup variant="flush">
              {this.props.history.map((invoice) => (
                <ListGroup.Item action onClick={() => this.viewHistoryDetail(invoice.id)} key={invoice.id}>
                  {invoice.timestamp}<br/>
                  {invoice.merchantName}<br/>
                  {invoice.fiatAmount} {invoice.fiatCurrency} {invoice.status}<br/>
                  {invoice.description}
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Card>
        </VisibleElement>
      </div>
    )
  }
}

PaymentHistory.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  history: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      merchantName: PropTypes.string.isRequired,
      merchantDesc: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      fiatAmount: PropTypes.string.isRequired,
      fiatCurrency: PropTypes.string.isRequired,
      orderId: PropTypes.string,
      status: PropTypes.string.required,
      timestamp: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
}

function mapStateToProps(state) {
  return {
    isFetching: state.recentHistory.isFetching,
    history: state.recentHistory.response.payments.map((invoice) => {
      const timestamp = new Date(invoice.created_at).toLocaleString()
      return {
        id: invoice.id,
        merchantName: invoice.created_by.name,
        merchantDesc: invoice.created_by.description,
        description: invoice.description,
        fiatAmount: invoice.quoted_amount,
        fiatCurrency: invoice.quoted_currency,
        orderId: invoice.order_id,
        status: invoice.status,
        timestamp
      }
    })
  }
}

export default connect(mapStateToProps)(PaymentHistory)
