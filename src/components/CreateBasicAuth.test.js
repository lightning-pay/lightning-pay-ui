import React from 'react';
import ReactDOM from 'react-dom'

import sinon from 'sinon'
import thunk from 'redux-thunk'
import { shallow, mount } from 'enzyme';
import configureStore from 'redux-mock-store'

import { Provider } from 'react-redux'

import CreateBasicAuthConnected, {CreateBasicAuth} from './CreateBasicAuth'
import * as actions from '../state/actions'

const mockStore = configureStore([thunk])

const stubs = {};

beforeAll(() => {
  stubs.createBasicAuth = sinon.stub(actions, 'createBasicAuth').returns(() => {
    return function(dispatch) {}
  })
  stubs.setVisibilityFilter = sinon.stub(actions, 'setVisibilityFilter').returns(() => {
    return {}
  })
})

afterAll(() => {
  stubs.createBasicAuth.restore()
  stubs.setVisibilityFilter.restore()
})

it('renders', () => {
  const state = {
    authenticated: {id: ''},
    usernameAvailable: {
        isFetching: false,
        error: '',
        response: {
            username: '',
            available: false
        }
    },
    visibilityFilter: actions.VisibilityFilters.SHOW_BASIC_AUTH
  }
  const store = mockStore(state);
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <CreateBasicAuthConnected/>
    </Provider>,
    div
  );
});

it('changes visibility filter to existing account on request', () => {
    const fakeDispatch = (fn) => {
        return fn;
    }
    const component = shallow((<CreateBasicAuth dispatch={fakeDispatch} checkedUsername="" checkedUsernameAvailable={false} checkedUsernameFetching={false} checkedUsernameError=""/>));
    component.find('#useExistingAccountLink').at(0).simulate('click');
    expect(stubs.setVisibilityFilter.called).toEqual(true)
})

it('prevents submit when credentials do not meet requirements', () => {
    const fakeDispatch = (fn) => {
        return fn;
    }
    const component = mount((<CreateBasicAuth dispatch={fakeDispatch} checkedUsername="" checkedUsernameAvailable={false} checkedUsernameFetching={false} checkedUsernameError=""/>));
    component.find('#submitCreateBasicAuth').at(0).simulate('click', { preventDefault() {} });
    expect(stubs.createBasicAuth.called).toEqual(false)
})

it('allows submit when credentials do meet requirements', () => {
    const fakeDispatch = (fn) => {
        return fn;
    }
    const component = mount((<CreateBasicAuth dispatch={fakeDispatch} checkedUsername="fakeusername" checkedUsernameAvailable={true} checkedUsernameFetching={false} checkedUsernameError=""/>));

    component.find('input').at(1).simulate('change', {target: {value: "s"}})
    component.find('input').at(1).simulate('change', {target: {value: ""}})
    component.find('input').at(1).simulate('change', {target: {value: "password"}})
    component.find('input').at(1).simulate('change', {target: {value: "arc@de543"}})
    component.find('input').at(1).simulate('change', {target: {value: "arc@de543%"}})
    component.find('input').at(1).simulate('change', {target: {value: "arc@de543%1"}})
    component.find('input').at(1).simulate('change', {target: {value: "s345Mge!vfre3453453789SKMr"}})
    component.find('#submitCreateBasicAuth').at(0).simulate('click', { preventDefault() {} });
    
    expect(stubs.createBasicAuth.called).toEqual(true)
})