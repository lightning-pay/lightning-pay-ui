import React from 'react';
import ReactDOM from 'react-dom';
import PaymentAccepted from './PaymentAccepted';

import sinon from 'sinon'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import { Provider } from 'react-redux'

import * as actions from '../state/actions'

const mockStore = configureStore([thunk])

const stubs = {};

beforeAll(() => {
  stubs.setVisibilityFilter = sinon.stub(actions, 'setVisibilityFilter').returns(() => {
    return {}
  })
})

afterAll(() => {
  stubs.setVisibilityFilter.restore()
})

it('renders', () => {
  const state = {}
  const store = mockStore(state);
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <PaymentAccepted/>
    </Provider>,
    div
  );
});