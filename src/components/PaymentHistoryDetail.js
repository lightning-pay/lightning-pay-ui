import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'

import * as actions from '../state/actions'

class PaymentHistoryDetail extends Component {
  constructor(props) {
    super(props)
    this.closeHistoryDetail = this.closeHistoryDetail.bind(this)
  }

  closeHistoryDetail() {
    this.props.dispatch(actions.closeHistoryDetail())
  }

  render() {
    return (
       <Card className="text-center">
        <Card.Header style={{textAlign: "center"}}>Payment Detail</Card.Header>
        <Card.Body>
            <Card.Text>
              {this.props.timestamp}<br/>
              {this.props.merchantName}<br/>
              Order #: {this.props.orderId}
            </Card.Text>
            <Card.Text>
              {this.props.description}<br/>
            </Card.Text>
            <Card.Text>
              Amount: {this.props.fiatAmount} {this.props.fiatCurrency}<br/>
              Amount: {this.props.cryptoAmount} {this.props.cryptoCurrency}<br/>
              Rate: 1 {this.props.cryptoCurrency} = {this.props.conversionRate} {this.props.fiatCurrency}
            </Card.Text>
            <Card.Text>
              Status: {this.props.status}
            </Card.Text>
            <Button size="lg" variant="outline-danger" onClick={this.closeHistoryDetail}>Close</Button>
        </Card.Body>
      </Card>
    )
  }
}

PaymentHistoryDetail.propTypes = {
  id: PropTypes.string.isRequired,
  merchantName: PropTypes.string.isRequired,
  merchantDesc: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  fiatAmount: PropTypes.string.isRequired,
  fiatCurrency: PropTypes.string.isRequired,
  cryptoAmount: PropTypes.string.isRequired,
  cryptoCurrency: PropTypes.string.isRequired,
  orderId: PropTypes.string,
  status: PropTypes.string.isRequired,
  conversionRate: PropTypes.string.isRequired,
  timestamp: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  console.debug('PaymentHistoryDetail state', state.historyDetail, state.recentHistory)
  let match = {};
  state.recentHistory.response.payments.forEach((invoice) => {
    if(invoice.id === state.historyDetail) {
      const timestamp = new Date(invoice.created_at).toLocaleString()
      match = {
        id: invoice.id,
        merchantName: invoice.created_by.name,
        merchantDesc: invoice.created_by.description,
        description: invoice.description,
        fiatAmount: invoice.quoted_amount,
        fiatCurrency: invoice.quoted_currency,
        cryptoAmount: (invoice.satoshi / 1e8).toFixed(8),
        cryptoCurrency: 'BTC',
        orderId: invoice.order_id,
        status: invoice.status,
        conversionRate: invoice.quoted_rate.toFixed(2),
        timestamp
      }
    }
  })
  return match
}

export default connect(mapStateToProps)(PaymentHistoryDetail)
