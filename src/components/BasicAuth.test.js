import React from 'react';
import ReactDOM from 'react-dom'

import sinon from 'sinon'
import thunk from 'redux-thunk'
import { shallow, mount } from 'enzyme';
import configureStore from 'redux-mock-store'

import { Provider } from 'react-redux'

import BasicAuthConnected, {BasicAuth} from './BasicAuth'
import * as actions from '../state/actions'

const mockStore = configureStore([thunk])

const stubs = {};

beforeAll(() => {
  stubs.basicAuth = sinon.stub(actions, 'basicAuth').returns(() => {
    return function(dispatch) {}
  })
  stubs.setVisibilityFilter = sinon.stub(actions, 'setVisibilityFilter').returns(() => {
    return {}
  })
})

afterAll(() => {
  stubs.basicAuth.restore()
  stubs.setVisibilityFilter.restore()
})

it('renders', () => {
  const state = {
    authenticated: {id: ''},
    visibilityFilter: actions.VisibilityFilters.SHOW_BASIC_AUTH
  }
  const store = mockStore(state);
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <BasicAuthConnected/>
    </Provider>,
    div
  );
});

it('changes visibility filter to create account on request', () => {
    const fakeDispatch = (fn) => {
        return fn;
    }
    const component = shallow((<BasicAuth dispatch={fakeDispatch}/>));
    component.find('#createAccountLink').at(0).simulate('click');
    expect(stubs.setVisibilityFilter.called).toEqual(true)
})

it('calls basicAuth action on submit', () => {
    const fakeDispatch = (fn) => {
        return fn;
    }
    const component = mount((<BasicAuth dispatch={fakeDispatch}/>));
    component.find('#submitBasicAuth').at(0).simulate('click', { preventDefault() {} });
    expect(stubs.basicAuth.called).toEqual(true)
})