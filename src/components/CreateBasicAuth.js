import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import zxcvbn from 'zxcvbn'
import debounce from 'lodash.debounce'

import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import ProgressBar from 'react-bootstrap/ProgressBar'

import {checkUsernameAvailable, createBasicAuth, setVisibilityFilter, VisibilityFilters} from '../state/actions'

export class CreateBasicAuth extends Component {
  constructor(props) {
    super(props)
    this.handlePasswordUpdate = this.handlePasswordUpdate.bind(this);
    this.handleUsernameCheck = debounce(this.handleUsernameCheck.bind(this), 500);
    this.submitForm = this.submitForm.bind(this)
    this.useExistingAccount = this.useExistingAccount.bind(this)

    this.state = {
      username: {
        note: ''
      },
      password: {
        score: 0,
        label: '',
        variant: 'danger',
        percent: 0
      },
      submitting: false
    };
  }

  async handleUsernameCheck(username) {
    const usernameRegex = /^[a-z0-9]+$/
    const usernameValid = username.match(usernameRegex)

    console.log(username, usernameValid)

    if (!usernameValid) {
      this.setState((prevState) => {
        return Object.assign(prevState, {
          username: {
            note: 'lowercase letters and numbers only'
          }
        })
      })
    } else if (username.length < 4) {
      this.setState((prevState) => {
        return Object.assign(prevState, {
          username: {
            note: 'too short'
          }
        })
      })
    } else {
      this.setState((prevState) => {
        return Object.assign(prevState, {
          username: {
            note: ''
          }
        })
      })
      await this.props.dispatch(checkUsernameAvailable(username));
    }
  }

  handlePasswordUpdate(event) {
    event.preventDefault();
    event.persist()
    this.setState((prevState) => {
      const text = event.target.value
      const result = zxcvbn(text)
      let label = ''
      let variant = ''
      let percent = 0

      if (result.score === 0) {
        label = 'very weak'
        variant = 'danger'
        percent = 20
      } else if (result.score === 1) {
        label = 'weak'
        variant = 'danger'
        percent = 40
      } else if (result.score === 2) {
        label = 'ok'
        variant = 'warning'
        percent = 60
      } else if (result.score === 3) {
        label = 'strong'
        variant = 'success'
        percent = 80
      } else if (result.score === 4) {
        label = 'very strong'
        variant = 'success'
        percent = 100
      }

      if (text.length === 0) {
        label = ''
        variant = 'danger'
        percent = 0
      }

      if (text.length < 8) {
        label = 'too short'
        variant = 'danger'
        percent = 40
      }

      return Object.assign(prevState, {
        password: {
          text,
          score: result.score,
          label,
          variant,
          percent
        }
      });
    })
  }

  submitForm(event) {
    event.preventDefault();
    this.setState((prevState) => {
      return Object.assign(prevState, {submitting: true})
    })
    this.props.dispatch(createBasicAuth(this.refs.username.value, this.refs.password.value))
  }

  useExistingAccount() {
    this.props.dispatch(setVisibilityFilter(VisibilityFilters.SHOW_BASIC_AUTH))
  }

  render() {
    let userNote = '';
    if (this.props.checkedUsername && !this.props.checkedUsernameAvailable) {
      userNote = 'taken'
    }
    
    let displayUsernameNote = 'none';
    if (this.state.username.note || userNote) {
      displayUsernameNote = 'block';
    }
    let usernameIsInvalid = false;
    let usernameIsValid = false;
    if(this.state.username.note || (this.props.checkedUsername && !this.props.checkedUsernameAvailable)){
      usernameIsInvalid = true
    }
    if(!this.state.username.note && this.props.checkedUsernameAvailable){
      usernameIsValid = true
    }

    let passwordIsInvalid = false;
    let passwordIsValid = false;
    if (this.state.password.text && this.state.password.percent < 60) {
      passwordIsInvalid = true
    }
    if (this.state.password.percent >= 60) {
      passwordIsValid = true
    }

    let submitDisabled = false;
    if (this.state.password.percent < 60 || this.state.username.note || this.props.checkedUsernameFetching || !this.props.checkedUsernameAvailable || this.state.submitting) {
      submitDisabled = true;
    }

    return (
      <Card className="text-center">
        <Card.Header>Create Account</Card.Header>
        <Card.Body>
          <div style={{textAlign: "left"}}>
            <Form method="POST" action="/api/new-basic-auth-credentials">
              <Form.Group controlId="formBasicUsername">
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type="text"
                  ref="username"
                  placeholder="New username"
                  onChange={() => {this.handleUsernameCheck(this.refs.username.value)}}
                  autoComplete="username"
                  isInvalid={usernameIsInvalid}
                  isValid={usernameIsValid}
                />
                <Form.Text className="text-muted" style={{display: displayUsernameNote}}>
                  {this.state.username.note}{userNote}
                </Form.Text>
              </Form.Group>
  
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  ref="password"
                  placeholder="Set Password"
                  onChange={this.handlePasswordUpdate}
                  autoComplete="new-password"
                  isInvalid={passwordIsInvalid}
                  isValid={passwordIsValid}
                  />
                <ProgressBar variant={this.state.password.variant} now={this.state.password.percent} label={this.state.password.label} style={{marginTop: "2px"}}/>
              </Form.Group>

              <div style={{textAlign: "center"}}>
                <Button id="submitCreateBasicAuth" variant="primary" size="lg" type="submit" style={{margin: "10px"}} disabled={submitDisabled} onClick={this.submitForm}>
                  Continue
                </Button>
                <br/>
                <Button id="useExistingAccountLink" variant="link" onClick={this.useExistingAccount}>Existing account? Sign In</Button>
              </div>
            </Form>
          </div>
        </Card.Body>
      </Card>
    )
  }
}

CreateBasicAuth.propTypes = {
  checkedUsername: PropTypes.string.isRequired,
  checkedUsernameAvailable: PropTypes.bool.isRequired,
  checkedUsernameFetching: PropTypes.bool.isRequired,
  checkedUsernameError: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    checkedUsername: state.usernameAvailable.response.username,
    checkedUsernameAvailable: state.usernameAvailable.response.available,
    checkedUsernameFetching: state.usernameAvailable.isFetching,
    checkedUsernameError: state.usernameAvailable.error
  }
}

export default connect(mapStateToProps)(CreateBasicAuth)
