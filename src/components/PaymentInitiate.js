import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import QRCode from 'qrcode.react'

import * as actions from '../state/actions'

class InitiatePayment extends Component {
  constructor(props) {
    super(props)
    const now = new Date();
    const expires = new Date(props.expires);
    const secondsRemaining = Math.floor((expires - now) / 1000)
    this.state = {secondsRemaining}
    this.cancelInitPayment = this.cancelInitPayment.bind(this)
  }

  tick() {
    this.setState(prevState => ({
      secondsRemaining: prevState.secondsRemaining - 1
    }))
    if (this.state.secondsRemaining <= 0) {
      this.cancelInitPayment()
    }
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  cancelInitPayment() {
    this.props.dispatch(actions.cancelInitPayment())
  }

  render() {
    const cardNumberFormatted = this.props.cardNumber.match(/.{1,4}/g).join(" ")
    const expiresDate = new Date(this.props.expires).toLocaleString()
    return (
      <Card className="text-center">
          <Card.Header style={{textAlign: "center"}}>Scan from POS</Card.Header>
          <Card.Body>
              <QRCode
                  value={this.props.cardNumber}
                  size={240}
                  bgColor="#ffffff"
                  fgColor="#333333"
                  level="M"
                  includeMargin={false}
                  renderAs="svg"
              />
              <Card.Title><br/>{cardNumberFormatted}</Card.Title>
              <Card.Text>
                Expires: {expiresDate}
              </Card.Text>
              <div style={{textAlign: "center"}}>
                <Button
                  variant="outline-danger"
                  size="lg"
                  onClick={this.cancelInitPayment}
                >Cancel</Button>
              </div>
          </Card.Body>
      </Card>
    )
  }
}

InitiatePayment.propTypes = {
  cardNumber: PropTypes.string.isRequired,
  expires: PropTypes.string.isRequired,
}

function mapStateToProps(state) {
  return {
    cardNumber: state.card.response.number,
    expires: state.card.response.expires_at,
  }
}

export default connect(mapStateToProps)(InitiatePayment)
