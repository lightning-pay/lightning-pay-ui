import React, {Component} from 'react';
import { connect } from 'react-redux'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'

import * as actions from '../state/actions'

export class PaymentAccepted extends Component {
  constructor(props) {
    super(props)
    this.continue = this.continue.bind(this)
  }
  
  continue() {
    this.props.dispatch(actions.setVisibilityFilter(actions.VisibilityFilters.SHOW_PAYMENT_HISTORY))
  }

  render() {
    return (
      <Card className="text-center">
          <Card.Body>
              <span style={{fontSize: "6em", color: "Green"}}>
                  <i className="fas fa-check-circle"></i>
              </span>
              <Card.Text>
                  Payment Accepted
              </Card.Text>
              <div style={{textAlign: "center"}}>
              <Button
                  variant="primary"
                  size="lg"
                  onClick={this.continue}
              >Continue</Button>
              </div>
          </Card.Body>
      </Card>
    )
  }
}

export default connect()(PaymentAccepted)